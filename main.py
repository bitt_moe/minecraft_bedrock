__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "09.10.2023 19:21"

import os

from fastapi import FastAPI

from app.BedrockServer import BedrockServer
from app.BedrockServerDownloadService import BedrockServerDownloadService
from app.model.CommandModel import CommandModel


def download_server(version: str, server_path: str):
    download_service = BedrockServerDownloadService(version=version, folder=server_path)
    unziped_path = download_service.download()
    if unziped_path is None:
        print(f"!Error on download server version: {version}!")
    else:
        print(f"Successfully downloaded to {unziped_path}")


EXEC_FILE_PATH = os.environ.get("EXEC_FILE_PATH")

if EXEC_FILE_PATH is not None and EXEC_FILE_PATH != "":
    server = BedrockServer(exec_file_path=EXEC_FILE_PATH)
    server.start()
else:
    print("bedrock server not started")
app = FastAPI()


@app.post("/command")
async def command(command: CommandModel):
    if server is not None:
        server.send_command(command.command)
        return True
    else:
        return False

