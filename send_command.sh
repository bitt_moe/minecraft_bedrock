#!/bin/bash

command=$1

curl -X 'POST' 'http://0.0.0.0:8000/command' -H 'accept: application/json' -H 'Content-Type: application/json' -d "{ \"command\": \"$command\" }"
