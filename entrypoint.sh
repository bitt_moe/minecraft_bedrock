#!/bin/bash

echo "============================== release-notes ===================================================================="

cat release-notes.txt

echo "============================== server.properties ================================================================="

cat server.properties

echo -e "\nRun servers:\n"

uvicorn main:app --host 0.0.0.0 --port 8000
