__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "09.10.2023 19:48"

import unittest
from pathlib import Path

from app.BedrockServerDownloadService import BedrockServerDownloadService


class DownloadBedrockServerServiceTest(unittest.TestCase):

    def setUp(self):
        self.service = BedrockServerDownloadService(version="1.20.32.03", folder="/tmp/bedrock_test")

    def test_download(self):
        self.assertIsInstance(self.service.download(), Path, msg="Не удалось скачать файл")

