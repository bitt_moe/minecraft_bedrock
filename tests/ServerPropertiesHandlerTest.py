__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 18:30"

import os
import unittest
from pathlib import Path

from app.ServerPropertiesHandler import ServerPropertiesHandler


class ServerPropertiesHandlerTest(unittest.TestCase):

    def setUp(self):
        EXEC_FILE_PATH = "/home/bit/Загрузки/bedrock_servers/1.20.32.03/bedrock_server"
        root_folder = Path(EXEC_FILE_PATH).parent
        self.server_properties_path = root_folder / "server.properties"

    def test_file_load(self):
        ServerPropertiesHandler(
            server_properties_file_path=self.server_properties_path,
            server_properties_map_path="../resources/server.properties.map.json"
        ).handle()

    def test_env_handling(self):
        os.environ.update({f"{ServerPropertiesHandler.ENV_PREFIX}SERVER_PORT": "12345"})
        handler = ServerPropertiesHandler(
            server_properties_file_path=self.server_properties_path,
            server_properties_map_path="../resources/server.properties.map.json"
        )
        handler.handle()
