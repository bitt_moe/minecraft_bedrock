# Minecraft Bedrock by Docker 

A  minecraft bedrock server with web server

## How to run

Run: 
```docker run -v /path/to/your/worlds/folder:/opt/minecraft/worlds -p 19132:19132/udp -p 8000:8000/tcp registry.gitlab.com/bitt_moe/minecraft_bedrock:1.20.32.03```

## Available endpoints

### POST /command

Example: ```curl -X 'POST' 'http://0.0.0.0:8000/command' -H 'accept: application/json' -H 'Content-Type: application/json' -d '{ "command": "say somethink"}'```

***For now this endpoint is not secured***

---
Bedrock version is end of link https://minecraft.azureedge.net/bin-linux/bedrock-server- **1.20.32.03** .zip