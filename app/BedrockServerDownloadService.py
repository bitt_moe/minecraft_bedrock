__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "09.10.2023 18:52"

from pathlib import Path
from zipfile import ZipFile, is_zipfile

from requests import get


class BedrockServerDownloadService:

    DOWNLOAD_PATH_START = "https://minecraft.azureedge.net/bin-linux/bedrock-server-"

    USER_AGENT_HEADER = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }

    def __init__(self, version: str, folder: str):
        """
        :param version: example: 1.20.32.03
        :param folder: server root folder
        """
        self.version = version
        self.root_folder = folder

    def download(self) -> Path | None:
        """
        :return: unziped folder path
        """
        download_url = f"{self.DOWNLOAD_PATH_START}{self.version}.zip"
        file_path_string = f"{self.root_folder}/bedrock-server.v{self.version}.zip"
        file_path = Path(file_path_string)
        if file_path.exists():
            print(f"{__name__}: File ({file_path_string}) exists")
            return self.unzip(file_path)
        response = get(url=download_url, headers=self.USER_AGENT_HEADER)
        if response.status_code == 200:
            with open(file=file_path_string, mode="wb") as file:
                file.write(response.content)
            return self.unzip(file_path)
        else:
            print(f"{__name__}: response status code: {response.status_code}, response: {response}")
            return None

    def unzip(self, file_path: Path) -> Path | None:
        """
        :param file_path: zip file path
        :return: unziped folder path
        """
        unzip_folder = Path(f"{self.root_folder}/bedrock_servers/{self.version}")
        if unzip_folder.exists():
            print(f"{__name__}: Already unziped in {unzip_folder}")
            return unzip_folder
        if is_zipfile(file_path):
            with ZipFile(file=file_path) as zip_file:
                zip_file.extractall(path=unzip_folder)
            return unzip_folder
        else:
            print(f"{__name__}: {file_path} is not a zip file")
            return None
