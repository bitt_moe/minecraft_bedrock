__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "09.10.2023 21:59"

import subprocess
from pathlib import Path

from app.ServerPropertiesHandler import ServerPropertiesHandler


class BedrockServer:

    def __init__(self, exec_file_path: str):
        self.exec_file_path = exec_file_path
        self.root_folder = Path(self.exec_file_path).parent
        self.process: subprocess.Popen | None = None
        self.server_properties_handler = ServerPropertiesHandler((self.root_folder / "server.properties"))

    def start(self):
        self.server_properties_handler.handle()
        self.process = subprocess.Popen(
            args=[self.exec_file_path],
            cwd=str(self.root_folder),
            stdin=subprocess.PIPE
        )

    def stop(self):
        self.send_command("stop")
        self.process.stdin.close()
        self.process = None

    def send_command(self, command: str):
        if self.process.stdin is not None:
            self.process.stdin.write(f"{command}\n".encode("UTF-8"))
            self.process.stdin.flush()
        else:
            print(f"self.process.stdin is none")
