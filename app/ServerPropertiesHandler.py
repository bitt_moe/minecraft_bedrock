__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "09.10.2023 18:53"

import json
import os
from pathlib import Path
from typing import Any

from app.ServerPropertyValidator import ServerPropertyValidator
from app.model.ServerPropertyModel import ServerPropertyModel


class ServerPropertiesHandler:

    ENV_PREFIX = "SERVER_PROPERTY_"

    def __init__(
            self,
            server_properties_file_path: Path,
            server_properties_map_path: str = "resources/server.properties.map.json"
    ):
        self.__server_properties_file_path = server_properties_file_path
        self.__server_properties: list[ServerPropertyModel] = []
        self.__load_file(server_properties_map_path)

    @classmethod
    def __handle_env_value_type(cls, value: Any) -> Any:
        if value is None:
            return value
        try:
            value = int(value)
            if type(value) == int and str(value).count(".") == 1:
                value = float(value)
        except ValueError:
            value = str(value)
        return value

    def __load_file(self, server_properties_map_path: str):
        with open(server_properties_map_path, "r") as file:
            json_object: dict = json.load(fp=file)
            for key, value in json_object.items():
                server_property = ServerPropertyModel(
                    name=key,
                    alias=value['alias'],
                    value=value["default_value"],
                    allowed_values=value["allowed_values"],
                    custom_mapping=value["values_map"]
                )
                ServerPropertyValidator(server_property=server_property).validate(server_property.value)
                self.__server_properties.append(server_property)

    def __server_property_by_alias(self, alias_name: str) -> ServerPropertyModel | None:
        """
        :param alias_name: example: 'SERVER_PORT'
        :return: server property
        """
        for server_property in self.__server_properties:
            if alias_name in server_property.alias:
                return server_property
        return None

    def __edit_server_property_by_alias(self, alias_name: str, value: Any) -> ServerPropertyModel | None:
        value = self.__handle_env_value_type(value)
        server_property = self.__server_property_by_alias(alias_name)
        validator = ServerPropertyValidator(server_property=server_property)
        if server_property is not None:
            server_property.value = validator.validate(value)
            return server_property
        else:
            return None

    def __handle_alias(self, aliases: dict):
        """
        :param aliases: example {"ENV_VAR": "ENV_VAR_VALUE"}
        """
        for key, value in aliases.items():
            if key.startswith(self.ENV_PREFIX):
                self.__edit_server_property_by_alias(key.replace(self.ENV_PREFIX, ""), value)

    def __load_to_file(self):
        with open(self.__server_properties_file_path, "w") as file:
            for server_property in self.__server_properties:
                file.write(f"{str(server_property)} \n")

    def __check_required_values(self):
        required_properties = [server_property.name for server_property in self.__server_properties if server_property.value is None]
        if len(required_properties) > 0:
            raise Exception(f"Required server propertoes: {', '.join(required_properties)}")

    def handle(self):
        self.__handle_alias(dict(os.environ))
        self.__check_required_values()
        self.__load_to_file()
