__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 22:43"

from typing import Any

from app.model.ServerPropertyAllowedValueEnum import ServerPropertyAllowedValueEnum
from app.model.ServerPropertyModel import ServerPropertyModel


class ServerPropertyValidator:

    def __init__(self, server_property: ServerPropertyModel):
        self.__server_property = server_property

    def validate(self, value: Any) -> Any:
        property_invalid_error = f"For property '{self.__server_property.name}' value: {type(value)} = {value} is not valid."
        mapped_value = self.__server_property.custom_mapping.get(value, None)
        if mapped_value is None:
            for allowed_value in self.__server_property.allowed_values:
                if allowed_value.type is ServerPropertyAllowedValueEnum.ENUM_STRING and value == allowed_value.value:
                    return value
                elif allowed_value.type is ServerPropertyAllowedValueEnum.ANY_STRING:
                    if type(value) == str:
                        return value
                    elif len(self.__server_property.allowed_values) > 1:
                        continue
                    else:
                        raise Exception(f"{property_invalid_error} Reason: is not a string (str)")
                elif allowed_value.type is ServerPropertyAllowedValueEnum.BOOLEAN:
                    if type(value) == bool:
                        return value
                    elif len(self.__server_property.allowed_values) > 1:
                        continue
                    else:
                        raise Exception(f"{property_invalid_error} Reason: is not a booelan (bool)")
                elif allowed_value.type is ServerPropertyAllowedValueEnum.FLOAT_RANGE:
                    if type(value) == float and allowed_value.value.in_range(value):
                        return value
                    elif len(self.__server_property.allowed_values) > 1:
                        continue
                    else:
                        raise Exception(f"{property_invalid_error} Reason: is not a float or not in range {allowed_value.value}")
                elif allowed_value.type is ServerPropertyAllowedValueEnum.INT_RANGE:
                    if type(value) == int and allowed_value.value.in_range(value):
                        return value
                    elif len(self.__server_property.allowed_values) > 1:
                        continue
                    else:
                        raise Exception(f"{property_invalid_error} Reason: is not a integer (int) or not in range {allowed_value.value}")
            raise Exception(f"value: {type(value)} = {value} is not valid")
        else:
            return mapped_value
