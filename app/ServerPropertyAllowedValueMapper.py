__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 21:15"

from typing import Any

from app.model.FloatRange import FloatRange
from app.model.IntRange import IntRange
from app.model.ServerPropertyAllowedValue import ServerPropertyAllowedValue
from app.model.ServerPropertyAllowedValueEnum import ServerPropertyAllowedValueEnum


class ServerPropertyAllowedValueMapper:

    MAPPING_EXCEPTION = Exception("Error on mapping type to ServerPropertyAllowedValueEnum")

    LEVEL_NAME_REGEXP = r"/\n\r\t\f`?*\\<>|\":"

    @classmethod
    def __map_type(cls, value: Any) -> ServerPropertyAllowedValueEnum:
        if type(value) is str:
            value = str(value)
            if value.count("...") > 0:
                range_from = value.split("...")[0]
                range_to = value.split("...")[1]
                range_from = int(range_from) if range_from.count(".") <= 0 else float(range_from)
                range_to = int(range_to) if range_to.count(".") <= 0 else float(range_to)
                if type(range_from) == int and type(range_to) == int:
                    return ServerPropertyAllowedValueEnum.INT_RANGE
                elif type(range_from) == float and type(range_to) == float:
                    return ServerPropertyAllowedValueEnum.FLOAT_RANGE
                else:
                    raise cls.MAPPING_EXCEPTION
            elif value == "str":
                return ServerPropertyAllowedValueEnum.ANY_STRING
            else:
                return ServerPropertyAllowedValueEnum.ENUM_STRING
        elif type(value) is bool:
            return ServerPropertyAllowedValueEnum.BOOLEAN
        else:
            raise cls.MAPPING_EXCEPTION

    @classmethod
    def map(cls, allowed_value: Any) -> ServerPropertyAllowedValue:
        value_type = cls.__map_type(allowed_value)
        if value_type is ServerPropertyAllowedValueEnum.INT_RANGE:
            allowed_value = IntRange(
                from_range=int(str(allowed_value).split("...")[0]),
                to_range=int(str(allowed_value).split("...")[1])
            )
        elif value_type is ServerPropertyAllowedValueEnum.FLOAT_RANGE:
            allowed_value = FloatRange(
                from_range=float(str(allowed_value).split("...")[0]),
                to_range=float(str(allowed_value).split("...")[1])
            )
        elif value_type is ServerPropertyAllowedValueEnum.ANY_STRING:
            try:
                allowed_value = ""
            except TypeError:
                raise Exception(f"{str(allowed_value)} is not string (str)")
        return ServerPropertyAllowedValue(
            value=allowed_value,
            type=value_type
        )
