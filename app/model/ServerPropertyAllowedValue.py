__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 21:10"

from dataclasses import dataclass

from app.model.FloatRange import FloatRange
from app.model.IntRange import IntRange
from app.model.ServerPropertyAllowedValueEnum import ServerPropertyAllowedValueEnum


@dataclass
class ServerPropertyAllowedValue:
    value: str | bool | IntRange | FloatRange
    type: ServerPropertyAllowedValueEnum

    def __str__(self) -> str:
        return f"AllowedValue(type={self.type}, value={self.value})"
