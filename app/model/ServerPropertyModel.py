__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 20:40"

from dataclasses import dataclass
from typing import Any

from app.ServerPropertyAllowedValueMapper import ServerPropertyAllowedValueMapper
from app.model.FloatRange import FloatRange
from app.model.IntRange import IntRange
from app.model.ServerPropertyAllowedValue import ServerPropertyAllowedValue


@dataclass
class ServerPropertyModel:
    name: str
    alias: list[str]
    value: str | bool | IntRange | FloatRange | None
    allowed_values: list[ServerPropertyAllowedValue]
    custom_mapping: dict

    def __init__(self, name: str, alias: list[str], value: Any, allowed_values: list[Any], custom_mapping: dict):
        mapper = ServerPropertyAllowedValueMapper
        self.name = name
        self.alias = alias
        self.value = value
        self.allowed_values = [mapper.map(allowed_value) for allowed_value in allowed_values]
        self.custom_mapping = custom_mapping

    def __str__(self) -> str:
        return f"{self.name}={str(self.value).lower() if type(self.value) in [bool] else self.value}"

    def __repr__(self) -> str:
        return (f"ServerPropertyModel("
                f"name={self.name}, "
                f"alias=[{', '.join(self.alias)}], "
                f"value={self.value}, "
                f"allowed_values={', '.join([str(allowed_value) for allowed_value in self.allowed_values])}, "
                f"custom_mapping={self.custom_mapping}"
                f")")
