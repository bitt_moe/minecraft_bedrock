__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 20:40"

from enum import Enum


class ServerPropertyAllowedValueEnum(Enum):
    ANY_STRING = 1
    INT_RANGE = 2
    FLOAT_RANGE = 3
    BOOLEAN = 4
    ENUM_STRING = 5

