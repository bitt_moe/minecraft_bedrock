__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "12.10.2023 21:26"

from dataclasses import dataclass


@dataclass
class FloatRange:
    from_range: float
    to_range: float

    def __init__(self, from_range: float, to_range: float):
        if from_range == to_range:
            raise ValueError("from_range equals to_range")
        self.from_range = from_range
        self.to_range = to_range

    def in_range(self, value: float) -> bool:
        return self.from_range <= value <= self.to_range
