FROM python:3.11-bullseye

ARG BEDROCK_VERSION
ARG SERVER_FOLDER="/opt/minecraft"

ENV EXEC_FILE_PATH=""

WORKDIR /opt/minecraft

COPY . .

RUN pip install -r requirements.txt

RUN python -c "from main import download_server; download_server(version=\"$BEDROCK_VERSION\", server_path=\"/tmp\")"

EXPOSE 19132/udp
EXPOSE 8000/tcp

VOLUME ["/opt/minecraft/worlds"]

RUN mv /tmp/bedrock_servers/$BEDROCK_VERSION/* .

RUN chmod +x entrypoint.sh
RUN chmod +x bedrock_server

ENV EXEC_FILE_PATH="/opt/minecraft/bedrock_server"

ENTRYPOINT ["/opt/minecraft/entrypoint.sh"]
